import React from 'react';
import Grid from '@material-ui/core/Grid';

import CountdownContainer from './components/countdown/CountdownContainer';
import TimeContainer from './components/time/TimeContainer';
import SpeedContainer from './components/speed/SpeedContainer';

const App = (props) => {
    return (
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
        >
            <Grid item style={{ marginTop: 125 }}>
                <CountdownContainer />
                <TimeContainer />
                <SpeedContainer />
            </Grid>
        </Grid>
    );
};

export default App;