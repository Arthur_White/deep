import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { shallow } from 'enzyme';

import Speed from './Speed';

Enzyme.configure({ adapter: new Adapter() });

describe('Speed component', () => {
    const props = {
        speed: 1,
        setSpeed: jest.fn()
    };

    it('click on speed button', () => {
        const speed = shallow(<Speed {...props} />);
        speed.find('.button1').simulate('click');
        expect(props.setSpeed).toHaveBeenCalledTimes(1);
    });
});