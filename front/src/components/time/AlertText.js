import React, { useEffect, useState } from 'react';
import Typography from '@material-ui/core/Typography';

const AlertText = (props) => {
    const [blink, setBlink] = useState(true);

    useEffect(() => {
        if (props.timeLeft <= 10 * 1000 && props.timeLeft > 0) {
            setTimeout(() => {
                setBlink(!blink);
            }, 300);
        }
    });

    return (
        <Typography className='text' variant="h6">
            {props.timeLeft !== 0
                ? props.countdown / 2 >= props.timeLeft
                    ? props.timeLeft <= 20 * 1000
                        ? props.timeLeft <= 10 * 1000
                            ? <div className='blink' style={{ color: 'red' }}>{blink && <i>More then halfway there!</i>}</div>
                            : <div className='red' style={{ color: 'red' }}><i>More then halfway there!</i></div>
                        : <i>More then halfway there!</i>
                    : null
                : <i>Time’s up!</i>}
        </Typography>
    );
};

export default AlertText;