import Time from './Time';
import { connect } from 'react-redux';
import { setIsRunFlag } from '../../redux/CountdownReducer';

const mapStateToProps = (state) => ({
    countdown: state.countdown.countdown,
    restart: state.countdown.restart,
    isRun: state.countdown.isRun,
    speed: state.speed.speed
});

export default connect(mapStateToProps, {
    setIsRunFlag,
})(Time);