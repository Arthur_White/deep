import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

const Countdown = (props) => {
    const [countdown, setCountdown] = React.useState({
        value: '',
        error: false
    });

    const validateInput = (event) => {
        if (event.target.value.match(/(^[0-9]{1,3}$)/)) {
            setCountdown({ value: event.target.value, error: false });
        } else {
            setCountdown({ value: '', error: true });
        }
    };

    const onStartClick = () => {
        if (!isNaN(parseInt(countdown.value))) {
            props.setCountdown(parseInt(countdown.value) * 60 * 1000);
            props.setIsRunFlag(true);
            props.setRestartFlag(!props.restart);
        }
    };

    return (
        <Grid
            container
            direction="row"
            justify="center"
            alignItems="center"
            spacing={2}
        >
            <Grid item>
                <Typography variant="h6">
                    Countdown:
                </Typography>
            </Grid>
            <Grid item>
                <TextField
                    className='input'
                    type="number"
                    min='0'
                    // variant="outlined"
                    placeholder={'(Min)'}
                    value={countdown.value}
                    error={countdown.error}
                    onChange={validateInput}
                />
            </Grid>
            <Grid item>
                <Button
                    className='button'
                    variant="contained"
                    style={{ background: 'green', color: 'white' }}
                    onClick={onStartClick}
                >
                    Start
                </Button>
            </Grid>
        </Grid>
    );
};

export default Countdown;