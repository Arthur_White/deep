This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### Start screen
![start screen](./media/start_screen.png)

### 1 min countdown
![1 min countdown](./media/timer_one_min.png)

### Halfway message
![halfway message](./media/halfway_message.png)

### 20 sec message
![20 sec message](./media/20_sec_message.png)

### 10 sec message (Blinking)
![10 sec message part 1](./media/10_sec_message.png)
![10 sec message part 1](./media/10_sec_message_2.png)

### Sound after time's up
![time's up sound](./media/sound.png)