const http = require('http');
const app = require('./app');

const server = http.createServer(app);
const PORT = process.env.PORT || 3000;
const pid = process.pid;

server.listen(PORT, () => {
    console.log('Server was started on port: ' + PORT + ' with PID: ' + pid);
});