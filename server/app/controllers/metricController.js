const config = require('config');
const metricAccessService = require('../services/metricAccessService');
const metricObjectCreatorService = require('../services/metricObjectCreatorService');

const addMetric = async (req, res) => {
    const key = req.params.key;
    const value = req.body.value ? req.body.value : config.get('defaultValueForPostRequest');

    const metricObj = metricObjectCreatorService.createMetricObject(key, value);
    metricAccessService.addNewMetricObj(metricObj);

    res.status(200);
    return res.json({});
};

const getMetricForPastHour = async (req, res) => {
    const key = req.params.key;
    const value = metricAccessService.getValueByMetricName(key);

    res.status(200);
    return res.json({ value });
};

module.exports = { addMetric, getMetricForPastHour };