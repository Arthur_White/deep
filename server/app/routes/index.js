const express = require('express');
const router = express.Router();

const metricRouter = require('./metricRouter');

router.use('/metric', metricRouter);

module.exports = router;