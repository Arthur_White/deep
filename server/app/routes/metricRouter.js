const express = require('express');
const router = express.Router();

const metricController = require('../controllers/metricController');

router.post('/:key', metricController.addMetric);
router.get('/:key/sum', metricController.getMetricForPastHour);

module.exports = router;