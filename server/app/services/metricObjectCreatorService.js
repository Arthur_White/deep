const createMetricObject = (metricName, metricValue) => {
    return {
        name: metricName,
        value: metricValue,
        date: new Date()
    };
};

module.exports = { createMetricObject };