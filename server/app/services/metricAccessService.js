const config = require('config');
const timeInterval = 60 * 60 * config.get('timeIntervalInHours') * 1000;

let metricsArray = require('../db');

const checkArrayForExpiredTime = () => {
    const dateToCheck = new Date().getTime() - timeInterval;
    metricsArray = metricsArray.filter(metric => metric.date.getTime() >= dateToCheck);
    return metricsArray;
};

const addNewMetricObj = (metricObj) => {
    metricsArray.unshift(metricObj);
    checkArrayForExpiredTime();
};

const getValueByMetricName = (metricName) => {
    let array = checkArrayForExpiredTime();
    array = array.filter(metric => metric.name === metricName).map(item => item.value);
    if (array.length > 0) {
        return parseInt(array.reduce((a, b) => a + b));
    } else {
        return 0;
    }
};

module.exports = { addNewMetricObj, getValueByMetricName };